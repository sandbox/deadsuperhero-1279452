This module uses JQuery to add captions to images.
The image alt attribute is used to create the image attribution. This is useful if you're running an article site and need to give credit to an image author. This module is based on the excellent "Image Caption" module by davidwhthomas. 

